from django.http import HttpResponse, Http404, HttpResponseRedirect

from django.urls import reverse_lazy

from django.shortcuts import render
from django.shortcuts import redirect

from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from django.views import View

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User

from django.contrib.staticfiles.storage import staticfiles_storage
from django.contrib.staticfiles.templatetags.staticfiles import static

from django.http import JsonResponse

from classes.models import Image
from classes.models import Label
from classes.models import Expert
from classes.models import *
from datetime import datetime

from . import forms
from . import highscore

from fs import open_fs
import random


from django.contrib.auth.decorators import login_required


def image_list(request):
    images = Image.objects.all()
    labels = Label.objects.all()
    context = {'object_list': images,
               'label_count': len(labels)}
    return render(request, 'classes/image_list.html', context)


@login_required
def image_folders(request):
    path = static('images/')
    path = staticfiles_storage.path('images/')
    print(path)
    img_root = open_fs(path)
    folders = img_root.listdir('/')
    context = {'folder_list': folders}
    return render(request, 'classes/image_folder_list.html', context)


@login_required
def load_images_from_folder(request, folder):
    path = staticfiles_storage.path('images/'+folder)
    img_folder = open_fs(path)
    files = sorted(img_folder.walk.files(filter=['*.png']))
    nr_created = 0
    for f in files:
        img, created = Image.objects.get_or_create(file_name=folder+f)
        if created:
            img.save()
            nr_created += 1

    context = {'folder': folder,
               'file_list': files,
               'nr_of_images': len(files),
               'nr_created': nr_created}
    return render(request, 'classes/image_folder_content.html', context)


def show_examples(request):
    subpath = 'examples/human'
    path = staticfiles_storage.path(subpath)
    img_folder = open_fs(path)
    files_human = sorted(img_folder.walk.files(filter=['*.png']))
    files_human = ["/static/"+subpath+s for s in files_human]

    subpath = 'examples/non_human'
    path = staticfiles_storage.path(subpath)
    img_folder = open_fs(path)
    files_non_human = sorted(img_folder.walk.files(filter=['*.png']))
    files_non_human = ["/static/"+subpath+s for s in files_non_human]

    subpath = 'examples/cutouts'
    path = staticfiles_storage.path(subpath)
    img_folder = open_fs(path)
    cutouts = sorted(img_folder.walk.files(filter=['*.png']))
    cutouts = ["/static/"+subpath+s for s in cutouts]

    context = {'humans': files_human,
               'non_humans': files_non_human,
               'cutouts': cutouts}

    return render(request, 'classes/examples.html', context)


def classification(request):
    img_file = Image.get_next()
    path = staticfiles_storage.url("images/"+str(img_file))
    context = {'image': img_file, 'file_path': path}
    return render(request, 'classes/classify.html', context)


@login_required
def mark_human(request, pk=None):
    if pk is not None:
        img = Image.objects.get(id=pk)
        label = Label(name="Human",
                      image=img,
                      expert=request.user)
        label.save()
        highscore.scores.increase_count(request.user)
    return redirect('index')


@login_required
def mark_non_human(request, pk=None):
    if pk is not None:
        img = Image.objects.get(id=pk)
        label = Label(name="Non-Human",
                      image=img,
                      expert=request.user)
        label.save()
        highscore.scores.increase_count(request.user)
    return redirect('index')


@login_required
def mark_undecided(request, pk=None):
    if pk is not None:
        img = Image.objects.get(id=pk)
        label = Label(name="Undecided",
                      image=img,
                      expert=request.user)
        label.save()
        highscore.scores.increase_count(request.user)
    return redirect('index')


class ExpertDetailView(LoginRequiredMixin, UpdateView):
    login_url = 'accounts/login/'
    redirect_field_name = 'next'
    model = Expert
    template_name = 'classes/expert_detail.html'
    form_class = forms.ExpertForm

    def get_object(self, *args, **kwargs):
        obj = super(ExpertDetailView, self).get_object(*args, **kwargs)
        if not obj.pk == self.request.user.pk:
            raise Http404
        return obj

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        return super().form_valid(form)


@login_required
def login_redirect(request):
    return redirect('expert-detail', pk=request.user.pk)


class LabelDeleteView(LoginRequiredMixin, DeleteView):
    login_url = 'accounts/login/'
    model = Label
    redirect_field_name = 'next'
    success_url = '/'

    def get_object(self, *args, **kwargs):
        obj = super(LabelDeleteView, self).get_object(*args, **kwargs)
        if not obj.expert.pk == self.request.user.pk:
            raise Http404
        print("hello")
        # highscore.scores.decrease_count(obj.expert)
        return obj

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.expert.pk == request.user.pk:
            highscore.scores.decrease_count(self.object.expert)
            self.object.delete()
            return HttpResponseRedirect(self.get_success_url())
        else:
            raise Http404


def ranking(request):
    context = {}
    return render(request, 'classes/ranking.html', context)


def export_labels(request):
    images = Image.objects.all()

    label_dict = {}
    for image in images:
        if image.get_labels():
            label_dict[image.file_name] = list(image.get_labels())

    return JsonResponse(label_dict)


def info(request):
    releaseDate = datetime(2019, 7, 9, 10, 0, 0, 0)
    end = datetime(2019, 7, 31, 23, 59, 59, 0)
    time_to_start = releaseDate - datetime.now()
    time_to_end = end - datetime.now()
    context = {'time_to_start': time_to_start,
               'time_to_end': time_to_end,
               }
    print(time_to_start)
    return render(request, 'classes/info.html', context)


def statistic_view(request):
    images_qs = get_all_images()
    singles_qs = get_single_lable_images()
    multis_qs = get_multiple_lable_images()
    no_lable_qs = get_unlabled_images()
    humans_qs = get_human_images()
    non_humans_qs = get_non_human_images()
    undecided_set = get_ambiguous_images()

    distribution = {}
    distribution["Human (one lable)"] = (singles_qs & humans_qs).count()
    distribution["Human (multiple lables)"] = (multis_qs & humans_qs).count()
    distribution["No Human (one lable)"] = (singles_qs & non_humans_qs).count()
    distribution["No Human (multiple lables)"] = (multis_qs & non_humans_qs).count()
    distribution["Undecided"] = undecided_set.count()
    distribution["Unlabled"] = no_lable_qs.count()

    context = {"distribution": distribution}
    return render(request, 'classes/statistics.html', context)
