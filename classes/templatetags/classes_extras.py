from django import template
from classes.models import Expert, Label, Image
from classes.highscore import scores
from django.http import Http404
register = template.Library()


@register.simple_tag
def get_user_labels(pk):
    u = Expert.objects.get(id=pk)
    return u.get_labels()


@register.simple_tag
def get_user_label_count(pk):
    return scores.get(pk)


@register.simple_tag
def get_highscore():
    return scores.highscore()


@register.simple_tag
def get_last_label_id(pk):
    expert = Expert.objects.get(id=pk)
    labels = expert.label_set.all().order_by('-date')
    if labels.count() > 0:
        return labels[0].id
    else:
        return 0


@register.simple_tag
def get_ranking():
    experts = Expert.objects.all()
    sorted_experts = sorted(experts, key=lambda e: e.get_score(), reverse=True)
    return sorted_experts


@register.simple_tag
def get_rank(pk):
    return scores.get_rank(pk)


@register.simple_tag
def get_diff_to_next(pk):
    return scores.get_diff_to_next(pk)


@register.simple_tag
def get_total_label_count():
    return Label.objects.all().count()


@register.simple_tag
def get_total_image_count():
    return Image.objects.all().count()
