from django.db import models
from django.db.models import Max, Count, Q, F
from django.urls import reverse
from django.contrib.auth.models import User
import random
import timeit
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings


class Image(models.Model):
    file_name = models.CharField(max_length=200)

    def get_absolute_url(self):
        return reverse('image-detail', args=[str(self.id)])

    def get_labels(self):
        u = Image.objects.get(pk=self.id)
        labels = u.label_set.all().values('name')
        return labels

    def has_labels(self):
        if self.get_labels().count() > 0:
            return True
        else:
            return False

    def get_full_path(self):
        return "images/"+self.file_name

    def __str__(self):
        return self.file_name

    queue = []
    queue_limit = 2000

    @classmethod
    def get_next(cls):
        """Returns the next image for classification."""
        try:
            return cls.queue.pop()
        except IndexError:
            # Queue is empty.
            pass

        new_queue = list(get_images()[:cls.queue_limit])
        assert len(new_queue) > 0
        random.shuffle(new_queue)
        i = new_queue.pop()
        cls.queue = new_queue
        return i


def get_all_images():
    return Image.objects.all()


def get_all_images_with_lable_counts():
    images = get_all_images()

    # add total lable count to every image
    images = images.annotate(nr_of_lables=Count('label'))

    # add nr of human/non-human/undecided lables to every image
    human_lables = Count('label', filter=Q(label__name__exact="Human"))
    non_human_lables = Count('label', filter=Q(label__name__exact="Non-Human"))
    undecided_lables = Count('label', filter=Q(label__name__exact="Undecided"))
    images = images.annotate(
        nr_of_human_lables=human_lables
    ).annotate(
        nr_of_non_human_lables=non_human_lables
    ).annotate(
        nr_of_undecided_lables=undecided_lables)

    return images


def get_unlabled_images():
    images = get_all_images_with_lable_counts()
    return images.filter(nr_of_lables=0)


def get_images_with_least_lables():
    return get_all_images_with_lable_counts().order_by('nr_of_lables')


def get_single_lable_images():
    images = get_all_images_with_lable_counts()
    return images.filter(nr_of_lables=1)


def get_multiple_lable_images():
    images = get_all_images_with_lable_counts()
    return images.filter(nr_of_lables__gt=1)


def get_human_images():
    images = get_all_images_with_lable_counts()

    # all images that have more non human labels than any other lables
    images = images.filter(
        nr_of_human_lables__gt=F('nr_of_non_human_lables')
    ).filter(
        nr_of_human_lables__gt=F('nr_of_undecided_lables')
    )

    return images


def get_non_human_images():
    images = get_all_images_with_lable_counts()

    # all images that have more non human labels than any other lables
    images = images.filter(
        nr_of_non_human_lables__gt=F('nr_of_human_lables')
    ).filter(
        nr_of_non_human_lables__gt=F('nr_of_undecided_lables')
    )

    return images


def get_ambiguous_images():
    images = get_all_images_with_lable_counts()

    # only take images that already have lables
    images = images.filter(nr_of_lables__gt=0)

    # all images that have at least as many undecided lables as other lables
    undecided_images = images.filter(
        nr_of_undecided_lables__gte=F('nr_of_human_lables')
    ).filter(
        nr_of_undecided_lables__gte=F('nr_of_non_human_lables')
    )

    # all images that have the same nr of human and non_human lables
    tied_images = images.filter(
        nr_of_human_lables=F('nr_of_non_human_lables')
    )

    images = undecided_images.union(tied_images)
    return images


def get_images():
    # Switch between different image selection policys
    if settings.IMAGE_SELECTION_POLICY == "least_labled":
        qs = get_images_with_least_lables()
        print("IMAGE_SELECTION_POLICY: least_labled. Returned", qs.count(), "images.")
    elif settings.IMAGE_SELECTION_POLICY == "ambigous_labled":
        qs = get_ambiguous_images()
        print("IMAGE_SELECTION_POLICY: ambigous_labled. Returned", qs.count(), "images.")
    else:
        qs = get_images_with_least_lables()
        print("No IMAGE_SELECTION_POLICY defined in settings file. Selecting least_labled images.")

    # Now make sure we have at least one.
    if qs.count() == 0:
        qs = get_all_images()
        print("IMAGE_SELECTION_POLICY did not return any images. Returning all images as a fallback.")
    return qs


class Label(models.Model):
    name = models.CharField(max_length=200)
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    expert = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)

    def get_absolute_url(self):
        return reverse('label-detail', args=[str(self.id)])

    def get_images(self):
        u = Label.objects.get(pk=self.id)
        images = u.image_set.all()
        return images

    def __str__(self):
        return self.name


class Expert(User):

    class Meta:
        proxy = True

    def get_labels(self):
        u = Expert.objects.get(pk=self.id)
        labels = u.label_set.all().order_by('-date')
        return labels

    def get_score_bonus(self):
        u = Expert.objects.get(pk=self.id)
        try:
            bonus_obj = u.scorebonus
        except ObjectDoesNotExist:
            bonus_obj = ScoreBonus(expert=self)
            bonus_obj.save()

        return bonus_obj

    def get_absolute_url(self):
        return reverse('expert-detail', args=[str(self.id)])

    def get_score(self):
        return self.get_labels().count() + self.get_score_bonus().get_value()


class ScoreBonus(models.Model):
    expert = models.OneToOneField(
        Expert,
        on_delete=models.CASCADE,
        null=True
    )
    rubber_score = models.IntegerField(default=0)

    def get_value(self):
        # returns accumulated score boni
        bonus = 0
        bonus += self.rubber_score

        return bonus

    def get_absolute_url(self):
        return reverse('scorebonus-detail', args=[str(self.id)])

    def increase_rubber_score(self, val: int):
        self.rubber_score += val
        self.save()

    def decrease_rubber_score(self, val: int):
        self.rubber_score -= val
        self.save()
