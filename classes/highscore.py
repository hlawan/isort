from .models import Expert, Label, ScoreBonus
from django.conf import settings


class Scores(object):
    """Keeps track of scores.

    Keeps scores in memory so that we don't have to query the database.

    """

    def __init__(self):
        self.counts = dict()
        for e in Expert.objects.all():
            self.counts[e.pk] = e.get_score()

    def increase_count(self, expert):
        # get meta class object to have access to custom methods
        expert = Expert.objects.get(pk=expert.pk)

        try:
            score = self.counts[expert.pk]
        except KeyError:
            score = 0

        # get bonus before increase_count
        rubber_bonus = self.get_rubber_bonus(score)
        score += 1
        # save bonus in database
        expert.get_score_bonus().increase_rubber_score(rubber_bonus)

        score += rubber_bonus
        self.counts[expert.pk] = score

    def decrease_count(self, expert):
        # get meta class object to have access to custom methods
        expert = Expert.objects.get(pk=expert.pk)

        try:
            score = self.counts[expert.pk]
        except KeyError:
            score = 0

        # decrease first to get the bonus that was added before
        score -= 1
        rubber_bonus = self.get_rubber_bonus(score)
        # save bonus in database
        expert.get_score_bonus().decrease_rubber_score(rubber_bonus)

        score -= rubber_bonus
        score = max(score, 0)
        self.counts[expert.pk] = score

    def get(self, pk):
        try:
            return self.counts[pk]
        except KeyError:
            self.counts[pk] = 0

        return self.counts[pk]

    def get_ranking(self):
        pass

    def get_rank(self, pk):
        rank = 1
        my_score = self.get(pk)

        for (key, value) in self.counts.items():
            if key != pk and value > my_score:
                rank += 1

        return rank

    def get_diff_to_next(self, pk):
        ranking = sorted(self.counts.values(), reverse=True)
        if len(ranking) < 2:
            return "-"

        rank = self.get_rank(pk)
        if rank == 1:
            a = ranking[rank-1]
            b = ranking[rank]
        else:
            a = ranking[rank-2]
            b = ranking[rank-1]

        return a-b

    def highscore(self):
        return max(self.counts.values())

    def get_rubber_bonus(self, score):
        # bonus increases with the difference to the highscore
        diff_to_first = self.highscore() - score
        bonus = int(diff_to_first / settings.RUBBER_DISTANCE)
        # avoid negative bonus for highscore leader
        bonus = max(bonus, 0)
        return bonus


        # Singleton highscore list.
scores = Scores()
